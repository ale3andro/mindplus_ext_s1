\   
enum SIZE {
    //% block="29*29"
    1,
    //% block="58*58"
    2
}

enum DIGITAL_PORTS {
    //% block="D5"
    5,
    //% block="D6"
    6,
    //% block="D7"
    7,
    //% block="D8"
    8,
    //% block="D9"
    9
}

enum PWM_PORTS {
    //% block="D5"
    5,
    //% block="D6"
    6,
    //% block="D9"
    9
}

enum NEOPIXEL_SUB_LEDS {
    //% block="Όλα"
    4,
    //% block="1o"
    0,
    //% block="2o"
    1,
    //% block="3o"
    2,
    //% block="4o"
    3,
}

enum ANALOG_PORTS {
    //% block="A0"
    0,
    //% block="A1"
    1,
    //% block="A2"
    2,
    //% block="A3"
    3,
}

enum DIRECTIONS {
    //% block="Ρολογιού"
    HIGH,
    //% block="Αντίστροφη Ρολογιού"
    LOW
}

enum LED_STATES {
    //% block="Άναψε"
    HIGH,
    //% block="Σβήσε"
    LOW
}

enum BUZZER_TONES_DURATION {
    //% block="Μισό"
    500,
    //% block="Τέταρτο"
    250,
    //% block="Όγδοο"
    125,
    //% block="Ολόκληρο"
    1000,
    //% block="Διπλό"
    2000,
    //% block="Στοπ"
    0,
}

enum REMOTE_CODES {
    //% block="LG::Power"
    551489775,
    //% block="LG::Πάνω βέλος"
    551486205,
    //% block="LG::Κάτω βέλος"
    551518845,
    //% block="LG::Δεξί βέλος"
    551510175,
    //% block="LG::Αριστερό βέλος"
    551542815,
    //% block="LG::Φωνή +"
    551502015,
    //% block="LG::Φωνή -"
    551534655,
    //% block="LG::Κανάλι +"
    551485695,
    //% block="LG::Κανάλι -"
    551518335,  
}

enum BUZZER_TONES {
    //% block="Χαμηλό Ντο (C3)"
    131,
    //% block="Χαμηλό Ντο# (C#3)"
    139,
    //% block="Χαμηλό Ρε (D3)"
    147,
    //% block="Χαμηλό Ρε# (D#3)"
    156,
    //% block="Χαμηλό Μι (E3)"
    165,
    //% block="Χαμηλό Φα (C3)"
    175,
    //% block="Χαμηλό Φα# (F#3)"
    185,
    //% block="Χαμηλό Σολ (G3)"
    196,
    //% block="Χαμηλό Σολ# (G#3)"
    208,
    //% block="Χαμηλό Λα (A3)"
    220,
    //% block="Χαμηλό Λα# (A#3)"
    233,
    //% block="Χαμηλό Σι (B3)"
    247,
    //% block="Μεσαίο Ντο (C4)"
    262,
    //% block="Μεσαίο Ντο# (C#4)"
    277,
    //% block="Μεσαίο Ρε (D4)"
    294,
    //% block="Μεσαίο Ρε# (D#4)"
    311,
    //% block="Μεσαίο Μι (E4)"
    330,
    //% block="Μεσαίο Φα (F4)"
    349,
    //% block="Μεσαίο Φα# (F#4)"
    370,
    //% block="Μεσαίο Σολ (G4)"
    392,
    //% block="Μεσαίο Σολ# (G#4)"
    415,
    //% block="Μεσαίο Λα (A4)"
    440,
    //% block="Μεσαίο Λα# (A#4)"
    466,
    //% block="Μεσαίο Σι (B4)"
    494,
    //% block="Υψηλό Ντο (C5)"
    523,
    //% block="Υψηλό Ντο# (C#5)"
    554,
    //% block="Υψηλό Ρε (D5)"
    587,
    //% block="Υψηλό Ρε# (D#5)"
    622,
    //% block="Υψηλό Μι (E5)"
    659,
    //% block="Υψηλό Φα (F5)"
    698,
    //% block="Υψηλό Φα# (F#5)"
    740,
    //% block="Υψηλό Σολ (G5)"
    784,
    //% block="Υψηλό Σολ# (G#5)"
    831,
    //% block="Υψηλό Λα (A5)"
    880,
    //% block="Υψηλό Λα# (A#5)"
    932,
    //% block="Υψηλό Σι (B5)"
    988,
}

//% color="#1c0dbd" iconWidth=50 iconHeight=40
namespace alxS1 {

    //% block="Led στο pin [BUTTON] [STATE]" blockType="command"
    //% BUTTON.shadow="dropdown" BUTTON.options="DIGITAL_PORTS" BUTTON.defl="DIGITAL_PORTS.6"
    //% STATE.shadow="dropdown" STATE.options="LED_STATES" STATE.defl="LED_STATES.HIGH"
    export function setLedState(parameter: any, block: any) {
        let button = parameter.BUTTON.code;
        button = replace(button);
        let state = parameter.STATE.code;
        if(Generator.board === 'arduino'){
            Generator.addCode(`digitalWrite(${button}, ${state});`);
        }
    }

    //% block="Led στο pin [BUTTON] άναψε με φωτεινότητα [INTENSITY]" blockType="command"
    //% BUTTON.shadow="dropdown" BUTTON.options="PWM_PORTS" BUTTON.defl="PWM_PORTS.6"
    //% INTENSITY.shadow="range" INTENSITY.defl="255" INTENSITY.params.max="255"
    export function setLedIntensity(parameter: any, block: any) {
        let button = parameter.BUTTON.code;
        button = replace(button);
        let intensity=parameter.INTENSITY.code;

        if(Generator.board === 'arduino'){
            Generator.addCode(`analogWrite(${button}, ${intensity});`);
        }
    }

    //% block="Neopixel Led στο pin [PORT], [SUBLEDS] με χρώμα R: [R], G: [G], B: [B] με φωτεινότητα [BRIGHTNESS]" blockType="command"
    //% PORT.shadow="dropdown" PORT.options="DIGITAL_PORTS" PORT.defl="DIGITAL_PORTS.11"
    //% SUBLEDS.shadow="dropdown" SUBLEDS.options="NEOPIXEL_SUB_LEDS" SUBLEDS.defl="NEOPIXEL_SUB_LEDS.4"
    //% R.shadow="range" R.defl="255" R.params.max="255"
    //% G.shadow="range" G.defl="255" G.params.max="255"
    //% B.shadow="range" B.defl="255" B.params.max="255"
    //% BRIGHTNESS.shadow="range" BRIGHTNESS.defl="50" BRIGHTNESS.params.max="255"
    export function setNeopixel(parameter: any, block: any) {
        let port = parameter.PORT.code;
        let leds = parameter.SUBLEDS.code;
        let r = parameter.R.code;
        let g = parameter.G.code;
        let b = parameter.B.code;
        let brightness = parameter.BRIGHTNESS.code;
        if(Generator.board === 'arduino'){
            Generator.addInclude("Adafruit_Neopixel","#include <Adafruit_NeoPixel.h>");
            Generator.addObject(`Adafruit` ,`Adafruit_NeoPixel`,`pixels(4, ${port}, NEO_GRB + NEO_KHZ800);`);
            Generator.addSetup(`neopixel_setup`, `pixels.begin();`);
            if (leds==4){
                Generator.addCode(`pixels.setPixelColor(0, pixels.Color(${r}, ${g}, ${b}));`);
                Generator.addCode(`pixels.setPixelColor(1, pixels.Color(${r}, ${g}, ${b}));`);
                Generator.addCode(`pixels.setPixelColor(2, pixels.Color(${r}, ${g}, ${b}));`);
                Generator.addCode(`pixels.setPixelColor(3, pixels.Color(${r}, ${g}, ${b}));`);
            } else {
                Generator.addCode(`pixels.setPixelColor(${leds}, pixels.Color(${r}, ${g}, ${b}));`);
            }
            Generator.addCode(`pixels.setBrightness(${brightness});`)
            Generator.addCode(`pixels.show();`)
        }
    }

    //% block="Buzzer στο pin [BUTTON] παίξε νότα [TONE] για [DURATION]" blockType="command"
    //% BUTTON.shadow="dropdown" BUTTON.options="DIGITAL_PORTS" BUTTON.defl="DIGITAL_PORTS.6"
    //% TONE.shadow="dropdown" TONE.options="BUZZER_TONES" TONE.defl="BUZZER_TONES.262"
    //% DURATION.shadow="dropdown" DURATION.options="BUZZER_TONES_DURATION" DURATION.defl="BUZZER_TONES_DURATION.500"
    export function setBuzzerState(parameter: any, block: any) {
        let button = parameter.BUTTON.code;
        button = replace(button);
        let tone = parameter.TONE.code;
        let duration = parameter.DURATION.code;
        if(Generator.board === 'arduino'){
            Generator.addInclude("DFRobot_Tone","#include <DFRobot_Libraries.h>");
            Generator.addObject(`DFRobot_Tone_${button}` ,`DFRobot_Tone`,`alx_tone_${button}`);
            Generator.addCode(`alx_tone_${button}.play(${button}, ${tone}, ${duration});`);
        }
    }

    //% block="Laser στο pin [BUTTON] [STATE]" blockType="command"
    //% BUTTON.shadow="dropdown" BUTTON.options="DIGITAL_PORTS" BUTTON.defl="DIGITAL_PORTS.6"
    //% STATE.shadow="dropdown" STATE.options="LED_STATES" STATE.defl="LED_STATES.HIGH"
    export function setLaserState(parameter: any, block: any) {
        let button = parameter.BUTTON.code;
        button = replace(button);
        let state = parameter.STATE.code;
        if(Generator.board === 'arduino'){
            Generator.addCode(`digitalWrite(${button}, ${state});`);
        }
    }

    //% block="Σέρβο στο pin [BUTTON] όρισε γωνία [ANGLE]" blockType="command"
    //% BUTTON.shadow="dropdown" BUTTON.options="DIGITAL_PORTS" BUTTON.defl="DIGITAL_PORTS.9"
    //% ANGLE.shadow="range" ANGLE.defl="90" ANGLE.params.max="180"
    export function setServoAngle(parameter: any, block: any) {
        let button=parameter.BUTTON.code;
        let angle=parameter.ANGLE.code;
        if(Generator.board === 'arduino'){
            Generator.addInclude("DFRobot_servo","#include <DFRobot_Servo.h>");
            Generator.addObject(`servo_object_${button}` ,`Servo`,`servo_${button}`);
            Generator.addSetup(`servo_setup_${button}`, `servo_${button}.attach(${button});`);
            Generator.addCode(`servo_${button}.angle(${angle});`);
        }
    }

    //% block="Κινητήρας: φορά [DIRECTION] με ταχύτητα [SPEED]" blockType="command"
    //% DIRECTION.shadow="dropdown" DIRECTION.options="DIRECTIONS" DIRECTION.defl="DIRECTIONS.HIGH"
    //% SPEED.shadow="range" SPEED.defl="120" SPEED.params.min="0" SPEED.params.max="255"
    export function setMotorSpeed(parameter: any, block: any) {
        let direction=parameter.DIRECTION.code;  
        let speed=parameter.SPEED.code;
        let code = '/* this is a comment */';
        
        if(Generator.board === 'arduino'){
            Generator.addObject(`alx_value` ,`volatile int`,`alx_motor_speed`);
            if (direction=='LOW') {
                code = `alx_motor_speed = ${speed};`
            }
            else {
                code = `alx_motor_speed = 255 - ${speed};`
            }
            code = Generator.addCode([code, Generator.ORDER_UNARY_POSTFIX]);
            Generator.addCode(`delay(250);`);
            Generator.addCode(`digitalWrite(4, ${direction});`);
            Generator.addCode(`delay(250);`);
            Generator.addCode(`analogWrite(3, alx_motor_speed);`);
        }
    }

    //% block="IR Ανάγνωση από το pin [PORT]" blockType="command"
    //% PORT.shadow="dropdown" PORT.options="DIGITAL_PORTS" PORT.defl="DIGITAL_PORTS.6"
    export function enableIR(parameter: any, block: any) {
        let port=parameter.PORT.code;
        if(Generator.board === 'arduino'){
            Generator.addInclude("Arduino_IRremote","#include <IRremote.h>");
            Generator.addObject(`IRObject` ,`IRrecv`,`irrecv(${port})`);
            Generator.addObject(`IRResults` ,`decode_results`,`results`);
            Generator.addObject(`IRResultsInt` ,`unsigned long`,`key_value=0`);
            Generator.addSetup(`ir_setup0`, `irrecv.enableIRIn();`);
            Generator.addCode('if (irrecv.decode(&results)) { key_value=results.value; irrecv.resume(); } else { key_value=0; };')
        }
    }

    //% block="Διάβασε θερμοκρασία από το pin [BUTTON]" blockType="reporter"
    //% BUTTON.shadow="dropdown" BUTTON.options="DIGITAL_PORTS" BUTTON.defl="DIGITAL_PORTS.6"
    export function getTemperature(parameter: any, block: any) {
        let button = parameter.BUTTON.code;
        button = replace(button);
        if(Generator.board === 'arduino'){
            Generator.addInclude("DFRobot_DHT","#include <DFRobot_DHT.h>");
            Generator.addObject(`DHT_alx_${button}` ,`DFRobot_DHT`,`dht11_${button}`);
            Generator.addSetup(`DHT_alx_begin_${button}`, `dht11_${button}.begin(${button}, DHT11);`);
            Generator.addCode(`dht11_${button}.getTemperature()`);
        }
    }

    //% block="Διάβασε υγρασία από το pin [BUTTON]" blockType="reporter"
    //% BUTTON.shadow="dropdown" BUTTON.options="DIGITAL_PORTS" BUTTON.defl="DIGITAL_PORTS.6"
    export function getHumidity(parameter: any, block: any) {
        let button = parameter.BUTTON.code;
        button = replace(button);
        if(Generator.board === 'arduino'){
            Generator.addInclude("DFRobot_DHT","#include <DFRobot_DHT.h>");
            Generator.addObject(`DHT_alx_${button}` ,`DFRobot_DHT`,`dht11_${button}`);
            Generator.addSetup(`DHT_alx_begin_${button}`, `dht11_${button}.begin(${button}, DHT11);`);
            Generator.addCode(`dht11_${button}.getHumidity()`);
        }
    }

    //% block="Διάβασε επίπεδο φωτός από το pin [BUTTON]" blockType="reporter"
    //% BUTTON.shadow="dropdown" BUTTON.options="ANALOG_PORTS" BUTTON.defl="ANALOG_PORTS.0"
    export function getLightLevel(parameter: any, block: any) {
        let button = parameter.BUTTON.code;
        button = replace(button);
        if(Generator.board === 'arduino'){
            Generator.addCode(`analogRead(${button})`);
        }
    }

    //% block="Διάβασε ποτενσιόμετρο από το pin [BUTTON]" blockType="reporter"
    //% BUTTON.shadow="dropdown" BUTTON.options="ANALOG_PORTS" BUTTON.defl="ANALOG_PORTS.0"
    export function getPotensiometerLevel(parameter: any, block: any) {
        let button = parameter.BUTTON.code;
        button = replace(button);
        if(Generator.board === 'arduino'){
            Generator.addCode(`analogRead(${button})`);
        }
    }

    //% block="Διάβασε ένταση ήχου από το μικρόφωνο στο pin [BUTTON]" blockType="reporter"
    //% BUTTON.shadow="dropdown" BUTTON.options="ANALOG_PORTS" BUTTON.defl="ANALOG_PORTS.0"
    export function getSoundLevel(parameter: any, block: any) {
        let button = parameter.BUTTON.code;
        button = replace(button);
        if(Generator.board === 'arduino'){
            Generator.addCode(`analogRead(${button})`);
        }
    }    

    //% block="Αισθητήρας μαγνητικού πεδίου στο pin [PORT], ανιχνεύει μαγνήτη;" blockType="boolean"
    //% PORT.shadow="dropdown" PORT.options="DIGITAL_PORTS" PORT.defl="DIGITAL_PORTS.6"
    export function isMagneticSwitchNearMagnet(parameter: any, block: any) {
        let port=parameter.PORT.code;
        if(Generator.board === 'arduino'){
            Generator.addCode(`!digitalRead(${port})`);
        }
    }

    //% block="Κουμπί πίεσης στο pin [BUTTON], είναι πατημένο;" blockType="boolean"
    //% BUTTON.shadow="dropdown" BUTTON.options="DIGITAL_PORTS" BUTTON.defl="DIGITAL_PORTS.6"
    export function getPressButtonStatus(parameter: any, block: any) {
        let button=parameter.BUTTON.code;
        if(Generator.board === 'arduino'){
            Generator.addCode(`!digitalRead(${button})`);
        }
    }

    //% block="Κουμπί αφής στο pin [BUTTON], είναι πατημένο;" blockType="boolean"
    //% BUTTON.shadow="dropdown" BUTTON.options="DIGITAL_PORTS" BUTTON.defl="DIGITAL_PORTS.6"
    export function getTactileButtonStatus(parameter: any, block: any) {
        let button=parameter.BUTTON.code;
        if(Generator.board === 'arduino'){
            Generator.addCode(`digitalRead(${button})`);
        }
    }

    //% block="Αισθητήρας κίνησης στο pin [BUTTON]. Ανιχνεύτηκε κίνηση;" blockType="boolean"
    //% BUTTON.shadow="dropdown" BUTTON.options="DIGITAL_PORTS" BUTTON.defl="DIGITAL_PORTS.6"
    export function getPirSensorStatus(parameter: any, block: any) {
        let button=parameter.BUTTON.code;
        if(Generator.board === 'arduino'){
            Generator.addCode(`digitalRead(${button})`);
        }
    }

    //% block="ΙR πατήθηκε το κουμπί [REMOTE_CODE];" blockType="boolean"
    //% REMOTE_CODE.shadow="dropdown" REMOTE_CODE.options="REMOTE_CODES" REMOTE_CODE.defl="REMOTE_CODES.551489775"
    export function isIRCodeEqualTo(parameter: any, block: any) {
        let remoteCode = parameter.REMOTE_CODE.code;
        if(Generator.board === 'arduino'){
            Generator.addCode(`key_value==${remoteCode}`);
        }
    }

    //% block="Στείλε στο monitor app το [MESSAGE]" blockType="command"
    //% MESSAGE.shadow="string"
    export function sendToAlxArduinoMonitor(parameter: any, block: any) {
        let message = parameter.MESSAGE.code;
        if(Generator.board === 'arduino'){
            Generator.addSetup(`serial_setup`, `Serial.begin(115200);`);
            Generator.addCode(`Serial.println(${message});`);
            Generator.addCode(`delay(500);`);
        }
    }

    function replace(str :string) {
        return str.replace("+", "");
    }
}

